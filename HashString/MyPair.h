#ifndef PAIR_H
#define PAIR_H

#include <iostream>

typedef long long hashSingle;

class MyPair {
	hashSingle first;
	hashSingle second;

	public:
		MyPair() {}

		MyPair(hashSingle first, hashSingle second) : first(first), second(second) {}

		MyPair(const MyPair &other) : first(other.first), second(other.second) {}		

		hashSingle get_first() {
			return this->first;
		}

		hashSingle get_second() {
			return this->second;
		}

		void operator=(const MyPair &pair) {
		    this->first  = pair.first;
		    this->second = pair.second;		    
		}

		MyPair operator+(const MyPair &pair2) {
		    MyPair tmp_pair = *this;
		    tmp_pair.first  = tmp_pair.first  + pair2.first;
		    tmp_pair.second = tmp_pair.second + pair2.second;
		    return tmp_pair;
		}

		MyPair operator-(const MyPair &pair2) {
		    MyPair tmp_pair = *this;
		    tmp_pair.first  = tmp_pair.first  - pair2.first;
		    tmp_pair.second = tmp_pair.second - pair2.second;
		    return tmp_pair;
		}

		MyPair operator*(const MyPair &pair2) {
		    MyPair tmp_pair = *this;
		    tmp_pair.first  = tmp_pair.first  * pair2.first;
		    tmp_pair.second = tmp_pair.second * pair2.second;
		    return tmp_pair;
		}

		MyPair operator%(const MyPair &pair2) {
		    MyPair tmp_pair = *this;
		    tmp_pair.first  = tmp_pair.first  % pair2.first;
		    tmp_pair.second = tmp_pair.second % pair2.second;
		    return tmp_pair;
		}

		MyPair operator+(const hashSingle &single) {
		    MyPair tmp_pair = *this;
		    tmp_pair.first  = tmp_pair.first  + single;
		    tmp_pair.second = tmp_pair.second + single;
		    return tmp_pair;
		}

		MyPair operator-(const hashSingle &single) {
		    MyPair tmp_pair = *this;
		    tmp_pair.first  = tmp_pair.first  - single;
		    tmp_pair.second = tmp_pair.second - single;
		    return tmp_pair;
		}

		MyPair operator*(const hashSingle &single) {
		    MyPair tmp_pair = *this;
		    tmp_pair.first  = tmp_pair.first  * single;
		    tmp_pair.second = tmp_pair.second * single;
		    return tmp_pair;
		}

		MyPair operator%(const hashSingle &single) {
		    MyPair tmp_pair = *this;
		    tmp_pair.first  = tmp_pair.first  % single;
		    tmp_pair.second = tmp_pair.second % single;
		    return tmp_pair;
		}

		bool operator<(const MyPair &pair2) const {
			if (first < pair2.first) {
				return true;
			}
			if (first > pair2.first) {
				return false;
			}
			return second < pair2.second; 
		}	

		bool operator==(const MyPair &pair2) const {			
			return ((first == pair2.first) && (second == pair2.second)); 
		}	
		
		bool operator!=(const MyPair &pair2) const {			
			return ((first != pair2.first) || (second != pair2.second)); 
		}

		void print() {
			std::cout<<first<<" "<<second<<std::endl;
		}

    	friend std::ostream& operator<< (std::ostream& stream, const MyPair& pair) {
            stream << "(" << pair.first << ", " << pair.second << ")";
            return stream;
        }
};

#endif