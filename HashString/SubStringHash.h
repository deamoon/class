#ifndef HASH_H
#define HASH_H

#include "MyPair.h"

#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#include <map>
#include <stdio.h>

typedef MyPair hashType;
typedef int lengthType;

class SubStringHash {	
	hashType simpleNumber; // Q
	hashSingle bigModNumber; // P			
	hashType notCalc;

	lengthType length;
	std::vector<hashType> hash;	
	std::vector<hashType> cacheDegSimpleNumber;

	private:
		hashType getMod(hashType number) {				
	    	return (((number % bigModNumber) + bigModNumber) % bigModNumber);
		}
		
		hashType binPow(hashType number, lengthType deg) { // number^deg
			hashType res(1, 1);
			while (deg) {
				if (deg & 1) {
					res = getMod(number * res);
				}
				number = getMod(number * number);
				deg >>= 1;
			}
			return getMod(res);
		}

		hashType powSimpleNumber(lengthType deg) {			
			if (cacheDegSimpleNumber[deg] != notCalc) {
				return cacheDegSimpleNumber[deg];
			} else {
				hashType res = binPow(simpleNumber, deg);
				cacheDegSimpleNumber[deg] = res;	
				return res;
			}
		}

	public:
		SubStringHash(std::string str) {			
			simpleNumber = hashType(3518, 1049);
			bigModNumber = 2000*1000*1000;
			notCalc = hashType(-1, -1);
			length = str.length();
			hashType prefixHash(0ULL, 0ULL);
			for (lengthType indexSymbol = 0; indexSymbol < str.length(); ++indexSymbol) {
				prefixHash = getMod(prefixHash * simpleNumber + lengthType(str[indexSymbol]));
				hash.push_back(prefixHash);	
				cacheDegSimpleNumber.push_back(notCalc);			
			}
		}

		SubStringHash(char *str, lengthType len) {			
			simpleNumber = hashType(3518, 1049); 			
			bigModNumber = 2000*1000*1000;
			notCalc = hashType(-1, -1);
			length = len;
			hashType prefixHash(0, 0);
			for (lengthType indexSymbol = 0; indexSymbol < len; ++indexSymbol) {
				prefixHash = getMod(prefixHash * simpleNumber + (lengthType)(str[indexSymbol]));
				hash.push_back(prefixHash);
				cacheDegSimpleNumber.push_back(notCalc);				
			}
		}

		void addEndSymbol(char lastSymbol) {
			length += 1;
			cacheDegSimpleNumber.push_back(notCalc);
			hashType newHash = getMod(hash.back() * simpleNumber + lengthType(lastSymbol));
			hash.push_back(newHash);
		}
		
		hashType getSubHash(lengthType leftIndex, lengthType rightIndex) { // hash[l .. r)
        	if (leftIndex > rightIndex  || leftIndex >= length
        								|| rightIndex > length
        								|| rightIndex < 0
        								|| leftIndex < 0) {
        		char error_message[100];
        		sprintf(error_message, "antiCorrect indexes left = %d, right = %d", leftIndex, rightIndex);
        		throw std::invalid_argument(error_message);
        	}
        	if (leftIndex == rightIndex) {
        		return MyPair(0, 0);
        	}
        	hashType subHash;
        	if (leftIndex == 0) {
        		subHash = hash[rightIndex - 1];        		
        	} else {
				subHash = hash[rightIndex - 1] - hash[leftIndex-1] * powSimpleNumber(rightIndex - leftIndex);
        	}           	     
        	return getMod(subHash);
    	}
};

#endif