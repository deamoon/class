#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Main
#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>
#include <ctime>
#include <set>
#include <map>
#include <algorithm>
#include <stdio.h>
#include "SubStringHash.h"

using namespace std;

const int USUALLY_TEST_TIME = 100;
const bool DEBUG_MODE = true;
// USUALLY_TEST_TIME влияет на время выполнения тестов randomTest2, randomTest3

void printResultTimeTest(string name, clock_t a, int kolInit, int kolGet, int kolAdd) {
	clock_t b = clock();	
	BOOST_TEST_MESSAGE("Название теста: " << name);	
	BOOST_TEST_MESSAGE("Затрачено времени(в секундах) "<<(b-a)/CLOCKS_PER_SEC);
	BOOST_TEST_MESSAGE("Количество иницилизаций = " << kolInit);
	BOOST_TEST_MESSAGE("Количество getSubHash = " << kolGet);
	BOOST_TEST_MESSAGE("Количество addEndSymbol = " << kolAdd << "\n");
}

string nextAntiHashString(string s) {
	string reverse = "";
	for (int i = 0; i < s.length(); ++i) {		
		if (s[i] == 'A') {
			reverse += 'B';
		} else {
			reverse += 'A';
		}
	}	
	return s + reverse;
}

string randomString(int len, int size = 62) {		
    string alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    string s;
    for (int i = 0; i < len; ++i) {
        s += alphanum[rand() % (size - 1)];
    }
    return s;
}

void randomCstring(char *s, int len, int size = 62) {		
    string alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";    
    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (size - 1)];
    }    
}

void doubleRandomString(string &s1, string &s2, int len, int size = 62) {		
    s1 = randomString(len, size);
    s2 = randomString(len, size);
    if ((rand() % 5) == 0) {
    	for (int i=0;i<s1.length();++i) {
    		s2[i] = s1[i];
    	}
    }
}

BOOST_AUTO_TEST_CASE(simpleTest1) {
	SubStringHash hash1("sample");
	SubStringHash hash2("sample");
	BOOST_CHECK_EQUAL(hash1.getSubHash(0, 6), hash2.getSubHash(0, 6));
}

BOOST_AUTO_TEST_CASE(simpleTest2) {
	SubStringHash hash("abacaba");
	BOOST_CHECK_EQUAL(hash.getSubHash(0, 3), hash.getSubHash(4, 7));
}

BOOST_AUTO_TEST_CASE(simpleTest3) {
	SubStringHash hash1("abacaba");
	SubStringHash hash2("kbekrubacabawuef");
	BOOST_CHECK_EQUAL(hash1.getSubHash(2, 7), hash2.getSubHash(7, 12));
}

BOOST_AUTO_TEST_CASE(simpleTest4) {
	SubStringHash hash1("a");
	SubStringHash hash2("b");
	BOOST_CHECK(hash1.getSubHash(0, 1) != hash2.getSubHash(0, 1));	
} 
 
BOOST_AUTO_TEST_CASE(simpleTest5) {
	SubStringHash hash("abacaba");
	BOOST_CHECK_THROW(hash.getSubHash(2, 100), invalid_argument);	
} 

BOOST_AUTO_TEST_CASE(simpleTest6) {
	SubStringHash hash("abacaba");
	BOOST_CHECK_THROW(hash.getSubHash(-2, 5), invalid_argument);	
} 

BOOST_AUTO_TEST_CASE(simpleTest7) {
	SubStringHash hash("abacaba");
	BOOST_CHECK_THROW(hash.getSubHash(2, 1), invalid_argument);	
} 

BOOST_AUTO_TEST_CASE(simpleTest8) {
	SubStringHash hash("ab");
	BOOST_CHECK_THROW(hash.getSubHash(2, -3), invalid_argument);	
}

BOOST_AUTO_TEST_CASE(simpleTest9) {
	SubStringHash hash("ab");
	BOOST_CHECK_THROW(hash.getSubHash(2, 3), invalid_argument);	
}

BOOST_AUTO_TEST_CASE(simpleTest10) {
	SubStringHash hash1("abacaba");
	SubStringHash hash2("abac");
	hash2.addEndSymbol('a');
	hash2.addEndSymbol('b');
	hash2.addEndSymbol('a');
	for (int i = 0; i < 7; ++i) {
		for (int j = i; j < 7; ++j) {
			BOOST_CHECK_EQUAL(hash1.getSubHash(i, j), hash2.getSubHash(i, j));		
		}	
	}
}

BOOST_AUTO_TEST_CASE(antiHashTest) {
	clock_t a, b;
	a = clock();
	
	srand(888);
	string antiHashString = "A";	
	for (int i = 0; i < 20; ++i) {		
		antiHashString = nextAntiHashString(antiHashString);
	}
	for (int k = 3; k < 19; ++k) {		
		int st = 1 << k;
		string s1 = antiHashString.substr(0, st);
		string s2 = antiHashString.substr(st, st);		
		SubStringHash hash1(s1);
		SubStringHash hash2(s2);	
		if (s1 == s2) {
			BOOST_CHECK_EQUAL(hash1.getSubHash(0, s1.length()), hash2.getSubHash(0, s2.length()));
		} else {						
			BOOST_CHECK(hash1.getSubHash(0, s1.length()) != hash2.getSubHash(0, s2.length()));
		}
		b = clock();		
		BOOST_CHECK((b-a)/CLOCKS_PER_SEC < 10);
	}	
	
}

BOOST_AUTO_TEST_CASE(randomTest) {
	clock_t a, b;
	a = clock();
		
	srand(999);
	for (int i = 0; i < 1000; ++i) {
		string s1 = randomString(10000);
		string s2 = randomString(10000);	
		SubStringHash hash1(s1);
		SubStringHash hash2(s2);		
		SubStringHash hash3(s1);		
		BOOST_CHECK_EQUAL(hash1.getSubHash(0, s1.length()), hash3.getSubHash(0, s1.length()));
		if (s1 == s2) {
			BOOST_CHECK_EQUAL(hash1.getSubHash(0, s1.length()), hash2.getSubHash(0, s2.length()));
		} else {			
			BOOST_CHECK(hash1.getSubHash(0, s1.length()) != hash2.getSubHash(0, s2.length()));
		}
		b = clock();		
		BOOST_CHECK((b-a)/CLOCKS_PER_SEC < USUALLY_TEST_TIME);
	}
	
}

BOOST_AUTO_TEST_CASE(randomTest2) {
	clock_t a, b;	
	string s = randomString(1000);		
	a = clock();
	SubStringHash hash(s);

	for (int i1 = 0; i1 < s.length() - 1; ++i1) {		
		for (int j1 = i1 + 1; j1 < s.length(); ++j1) {		
			for (int i2 = 0; i2 < s.length() - 1; ++i2) {		
				for (int j2 = i2 + 1; j2 < s.length(); ++j2) {		
			
					if (j1-i1 == j2 - i2) { // Длины подстрок равны
						//
					} else {																	
						BOOST_CHECK(hash.getSubHash(i1, j1+1) != hash.getSubHash(i2, j2+1));											
					}	

					b = clock();					
					if ((b-a) / CLOCKS_PER_SEC > USUALLY_TEST_TIME) {
						return;					
					}

				}	
			}
		}			
	}	
}

BOOST_AUTO_TEST_CASE(randomTest3) {
	clock_t a, b;	
	string s = randomString(100, 10);	
	map<pair<int, int>, int> maxCommonPrefix;	
	a = clock();
	SubStringHash hash(s);	

	for (int len = 1; len <= s.length()-1; ++len) {		
		for (int l1 = 0; l1 <= s.length() - len; ++l1) {		
			for (int l2 = 0; l2 <= s.length() - len; ++l2) {													
				pair<int, int> p = make_pair(l1, l2);
				if (len == 1) {
					if (s[l1] == s[l2]) {
						maxCommonPrefix[p] = 1; 						
						BOOST_CHECK(hash.getSubHash(l1, l1+len) == hash.getSubHash(l2, l2+len));
					} else {
						BOOST_CHECK(hash.getSubHash(l1, l1+len) != hash.getSubHash(l2, l2+len));
					}					
				} else {
					if (maxCommonPrefix[p] == len - 1) {
						if (s[l1+len-1] == s[l2+len-1]) {
							maxCommonPrefix[p] = len;							
							BOOST_CHECK(hash.getSubHash(l1, l1+len) == hash.getSubHash(l2, l2+len));	
						} else {
							BOOST_CHECK(hash.getSubHash(l1, l1+len) != hash.getSubHash(l2, l2+len));
						}
					} else {
						BOOST_CHECK(hash.getSubHash(l1, l1+len) != hash.getSubHash(l2, l2+len));
					}
				}

				b = clock();
				if ((b-a) / CLOCKS_PER_SEC > USUALLY_TEST_TIME) {									
					return;					
				}
				
			}
		}			
	}	
}

BOOST_AUTO_TEST_CASE(timeTest1) {
	clock_t a, b;
	a = clock();
	int kol = 0;
	string s = randomString(1000000);
	SubStringHash hash(s);
	for (int i = 0; i < 1000000; ++i) {		
		hash.addEndSymbol(char(int('a') + i % 26));
		hashType p = hash.getSubHash(i % 1000, i % 10000 + 10001);
		kol += 1;
		b = clock();		
		BOOST_CHECK((b-a)/CLOCKS_PER_SEC < 5);		
	}
	printResultTimeTest("timeTest1", a, 1, kol, kol);		
}	

BOOST_AUTO_TEST_CASE(timeTest2) {
	clock_t a, b, c, d;
	a = clock();
	int kol = 0;
	for (int i = 0; i < 1000; ++i) {		
		c = clock();
		string s = randomString(20000);
		d = clock();

		SubStringHash hash(s);
		hash.addEndSymbol(char(int('a') + i % 26));
		hashType p = hash.getSubHash(i % 1000, i % 1000 + 1001);
		kol += 1;
		b = clock() - (i+1)*(d-c);		
		BOOST_CHECK((b-a)/CLOCKS_PER_SEC < 10);		
	}	
	printResultTimeTest("timeTest2", a, kol, kol, kol);			
}	

BOOST_AUTO_TEST_CASE(timeTest3) {
	clock_t a, b;
	string s = randomString(2000);
	a = clock();
	int kol = 0;
	SubStringHash hash(s);
	for (int i = 0; i < s.length()-1; ++i) {		
		for (int j = i+1; j < s.length()-2; ++j) {				
			hashType p = hash.getSubHash(i, j);
			kol += 1;
			b = clock();		
			BOOST_CHECK((b-a)/CLOCKS_PER_SEC < 10);						
		}
	}		
	printResultTimeTest("timeTest3", a, 1, kol, 0);			
}

BOOST_AUTO_TEST_CASE(compareTest1) {
	for (int i = 0; i < 1000000; ++i) {		
		string s1, s2;		
		doubleRandomString(s1, s2, 10);

		SubStringHash hash1(s1);
		SubStringHash hash2(s2);			
		if (s1 == s2) {
			BOOST_CHECK_EQUAL(hash1.getSubHash(0, s1.length()), hash2.getSubHash(0, s2.length()));
		} else {			
			BOOST_CHECK(hash1.getSubHash(0, s1.length()) != hash2.getSubHash(0, s2.length()));
		}		
	}	
}	

BOOST_AUTO_TEST_CASE(compareTest2) {
	for (int i = 0; i < 100000; ++i) {		
		string s1, s2;		
		doubleRandomString(s1, s2, 100);

		SubStringHash hash1(s1);
		SubStringHash hash2(s2);			
		if (s1 == s2) {
			BOOST_CHECK_EQUAL(hash1.getSubHash(0, s1.length()), hash2.getSubHash(0, s2.length()));
		} else {			
			BOOST_CHECK(hash1.getSubHash(0, s1.length()) != hash2.getSubHash(0, s2.length()));
		}		
	}	
}


BOOST_AUTO_TEST_CASE(compareTest3) {
	for (int i = 0; i < 1000; ++i) {		
		string s1, s2;		
		doubleRandomString(s1, s2, 1000);
		
		SubStringHash hash1(s1);
		SubStringHash hash2(s2);			
		if (s1 == s2) {
			BOOST_CHECK_EQUAL(hash1.getSubHash(0, s1.length()), hash2.getSubHash(0, s2.length()));
		} else {			
			BOOST_CHECK(hash1.getSubHash(0, s1.length()) != hash2.getSubHash(0, s2.length()));
		}		
	}	
}

BOOST_AUTO_TEST_CASE(containerTest1) {
	set<hashType> a;
	SubStringHash hash1("ab1");
	SubStringHash hash2("ab2");
	SubStringHash hash3("ab");
	hash3.addEndSymbol('3');
	SubStringHash hash4("ab1");
	SubStringHash hash5("ab2");
	SubStringHash hash6("ab");
	hash6.addEndSymbol('1');

	a.insert(hash1.getSubHash(0, 3));
	a.insert(hash2.getSubHash(0, 3));
	a.insert(hash3.getSubHash(0, 3));
	a.insert(hash4.getSubHash(0, 3));
	a.insert(hash5.getSubHash(0, 3));
	a.insert(hash6.getSubHash(0, 3));

	BOOST_CHECK_EQUAL(a.size(), 3);
}

BOOST_AUTO_TEST_CASE(containerTest2) {
	vector<hashType> a;
	for (int i = 0; i < 1000; ++i) {				
		SubStringHash hash(randomString(1000));
		a.push_back(hash.getSubHash(i % 123, i % 400 + 125));
	}
	sort(a.begin(), a.end());
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(containerTest3) {
	map<hashType, int> a;
	for (int i = 0; i < 1000; ++i) {				
		SubStringHash hash(randomString(1000));
		a[hash.getSubHash(i % 137, i % 400 + 150)] = i+1;
	}
	for (map<hashType, int>::iterator it = a.begin(); it != a.end(); ++it) {
		//
	}
	BOOST_CHECK(true);	
}

void printAboutBigMissing(const string &nameTest, int left, int len, pair<int, int> p) {
	BOOST_TEST_MESSAGE("Название теста " << nameTest);
	BOOST_TEST_MESSAGE("Упс, кажется хэши совпали =(");
	BOOST_TEST_MESSAGE("Индекс начала : " << left << " <> " << p.first);
	BOOST_TEST_MESSAGE("Длина : " << len << " <> "<< p.second << "\n");
}

bool isStringEquals(char *s, int left1, int len1, pair<int, int> p) {
	int left2 = p.first, len2 = p.second;
	if (len1 != len2) {
		return false;
	}
	for (int i = 0; i < len1; ++i) {
		if (s[left1 + i] != s[left2 + i]) {
			return false;
		}
	}
	return true;
}

// Идея : добавим все хэши различных строк в map и сравним его размер с
// кол-вом добавленного
pair<int, int> candyBox(const string &nameTest, char *s, int kol, int length = -1, int minLength = 1000, int maxLength = 1000*1000) {
	map< hashType, pair<int, int> > box;
		
	int kolTest = kol, len;
	SubStringHash hash(s);

	for (int i = 0; i < kolTest; ++i) {		
		int left = rand() % maxLength;
		
		if (length == -1) {
			len = rand() % maxLength + minLength;		
		} else {
			len = length;
		}	

		hashType h = hash.getSubHash(left, left + len); 
		
		if (box.count(h)) {		
			// Либо подстроки равны(вряд ли), либо printAboutBigMissing		
			if (isStringEquals(s, left, len, box[h])) {
				// Просто повезло					
				kol -= 1;
			} else {
				if (DEBUG_MODE) {
					printAboutBigMissing(nameTest, left, len, box[h]);
				}
			}						
		} else {
			box[h] = make_pair(left, len);						
		}					
	}

	return make_pair(box.size(), kol);
}

BOOST_AUTO_TEST_CASE(subStringTest1) {   
    char s[100*1000*1000];
    randomCstring(s, 100*1000*1000);    
	
	pair<int, int> p = candyBox("subStringTest1", s, 100*1000); // random length > 1000 and < 1000*1000  			 		
	BOOST_CHECK_EQUAL(p.first, p.second);
}

BOOST_AUTO_TEST_CASE(subStringTest2) {   
    char s[100*100*1000];
    randomCstring(s, 100*100*1000);
	pair<int, int> p = candyBox("subStringTest2", s, 100*1000, 10); // fix length = 1000    			 	
	BOOST_CHECK_EQUAL(p.first, p.second);
}

BOOST_AUTO_TEST_CASE(subStringTest3) {   
    char s[100*100*1000];
    randomCstring(s, 100*100*1000);
	pair<int, int> p = candyBox("subStringTest3", s, 100*1000, 1000); // fix length = 1000    			 	
	BOOST_CHECK_EQUAL(p.first, p.second);
}

BOOST_AUTO_TEST_CASE(subStringTest4) {   
    char s[100*100*1000];
    randomCstring(s, 100*100*1000);
	pair<int, int> p = candyBox("subStringTest4", s, 100*1000, 1000000); // fix length = 1000000
	BOOST_CHECK_EQUAL(p.first, p.second);
}

BOOST_AUTO_TEST_CASE(subStringTest5) {   
    char s[100*100*1000];
    randomCstring(s, 100*100*1000);
	pair<int, int> p = candyBox("subStringTest5", s, 100*1000, -1, 10, 1000); // random length > 10 and < 1010
	BOOST_CHECK_EQUAL(p.first, p.second);
}

void printResultTestOnSmallSizeAlphabet(string name, int eq, int notEq) {	
	BOOST_TEST_MESSAGE("Название теста: " << name);
	BOOST_TEST_MESSAGE("Проверено равных строк " << eq);
	BOOST_TEST_MESSAGE("Проверено неравных строк " << notEq << "\n");	
}

BOOST_AUTO_TEST_CASE(checkHashOnSmallSizeAlphabet1) {
	int kolEquals = 0;
	int kolNotEquals = 0;	
	for (int size = 2; size < 15; ++size) {		
		for (int i = 0; i < 1000; ++i) {		
			string s1, s2;		
			doubleRandomString(s1, s2, 100, size); // length = 100

			SubStringHash hash1(s1);
			SubStringHash hash2(s2);			
			if (s1 == s2) {
				++kolEquals;
				BOOST_CHECK_EQUAL(hash1.getSubHash(0, s1.length()), hash2.getSubHash(0, s2.length()));
			} else {			
				++kolNotEquals;
				BOOST_CHECK(hash1.getSubHash(0, s1.length()) != hash2.getSubHash(0, s2.length()));
			}			
		}	
	}	
	printResultTestOnSmallSizeAlphabet("checkHashOnSmallSizeAlphabet1", kolEquals, kolNotEquals);
}

BOOST_AUTO_TEST_CASE(checkHashOnSmallSizeAlphabet2) {
	int kolEquals = 0;
	int kolNotEquals = 0;	
	for (int size = 3; size < 5; ++size) {		
		for (int i = 0; i < 1000; ++i) {		
			string s1, s2;		
			doubleRandomString(s1, s2, 10000, size); // length = 10000

			SubStringHash hash1(s1);
			SubStringHash hash2(s2);			
			if (s1 == s2) {
				++kolEquals;
				BOOST_CHECK_EQUAL(hash1.getSubHash(0, s1.length()), hash2.getSubHash(0, s2.length()));
			} else {			
				++kolNotEquals;
				BOOST_CHECK(hash1.getSubHash(0, s1.length()) != hash2.getSubHash(0, s2.length()));
			}			
		}	
	}	
	printResultTestOnSmallSizeAlphabet("checkHashOnSmallSizeAlphabet2", kolEquals, kolNotEquals);
}

BOOST_AUTO_TEST_CASE(checkHashOnSmallSizeAlphabet3) {
	int kolEquals = 0;
	int kolNotEquals = 0;	
	for (int size = 3; size < 5; ++size) {		
		for (int i = 0; i < 100; ++i) {		
			string s1, s2;		
			doubleRandomString(s1, s2, 100000, size); // length = 1000000

			SubStringHash hash1(s1);
			SubStringHash hash2(s2);			
			if (s1 == s2) {
				++kolEquals;
				BOOST_CHECK_EQUAL(hash1.getSubHash(0, s1.length()), hash2.getSubHash(0, s2.length()));
			} else {			
				++kolNotEquals;
				BOOST_CHECK(hash1.getSubHash(0, s1.length()) != hash2.getSubHash(0, s2.length()));
			}			
		}	
	}	
	printResultTestOnSmallSizeAlphabet("checkHashOnSmallSizeAlphabet3", kolEquals, kolNotEquals);
}

BOOST_AUTO_TEST_CASE(lastTest1) {	
	int len = 50*1000;

	char s[1000*1000];
    randomCstring(s, 1000*1000, 20);			
	SubStringHash hash(s);
	map< hashType, pair<int, int> > box;
	int kol = 0;
	for (int left = 0; left < 1000*1000 - len; ++left) {					
		hashType h = hash.getSubHash(left, left + len); 		
		++kol;
		if (box.count(h)) {		
			// Либо подстроки равны(вряд ли), либо printAboutBigMissing		
			if (isStringEquals(s, left, len, box[h])) {
				// Просто повезло									
				--kol;
			} else {
				if (DEBUG_MODE) {
					printAboutBigMissing("lastTest", left, len, box[h]);
				}
			}						
		} else {
			box[h] = make_pair(left, len);						
		}
	}				
	BOOST_CHECK_EQUAL(box.size(), kol);
}

BOOST_AUTO_TEST_CASE(lastTest2) {	
	int len = 5*1000;

	char s[1000*1000];
    randomCstring(s, 1000*1000, 20);			
	SubStringHash hash(s);
	map< hashType, pair<int, int> > box;
	int kol = 0;
	for (int left = 0; left < 1000*1000 - len; ++left) {					
		hashType h = hash.getSubHash(left, left + len); 		
		++kol;
		if (box.count(h)) {		
			// Либо подстроки равны(вряд ли), либо printAboutBigMissing		
			if (isStringEquals(s, left, len, box[h])) {
				// Просто повезло									
				--kol;
			} else {
				if (DEBUG_MODE) {
					printAboutBigMissing("lastTest", left, len, box[h]);
				}
			}						
		} else {
			box[h] = make_pair(left, len);						
		}
	}				
	BOOST_CHECK_EQUAL(box.size(), kol);
}