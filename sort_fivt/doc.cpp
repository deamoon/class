#include <iostream>
#include <cstdlib>
#include<ctime>
#include <vector>
#include <algorithm>
using namespace std;

template<typename T>
void Test(){
  typedef void(*TGenerators vector<T> &);
  Tgenerators Gen[]={&genAccend,&genDeccend,&genEmpty,&genRandom}
  typedef void(*TComparators vector<T> &);
  TComparators Cmp[]={&cmpAccend,&cmpDeccend}
  typedef void(*TSortFuncs vector<T> &);
  TSortFuncs Sort[]={&sortSelect,&sortBubble,&sortHeap,&sortQuick}

   for (void *sort=Sort[0];sort<Sort.end();sort++) {//sortFunc in sortFuncs ... { // различные методы сортировок
       for (void *cmp=Cmp[0];cmp<Cmp.end();cmp++) {// различные направления сортировок
           for (void *gen=Gen[0];cmp<Gen.end();gen++) { // различные варианты инициализации вектора
              vector<T> v;
              (*gen)(v);
              (*sort)(v, cmp);
              Check(v, cmp);
           }
       }
   }
}
int main(){
  Test<int>();
  Test<double>();
  return 0;
}
