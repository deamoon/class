#include <iostream>
#include <cstdlib>
#include<ctime>
#include <vector>
#include <algorithm>
using namespace std;

/*template
bool Test(){
    typedef void(*TGenerator vector<T> &,size_t );
    TGenerator g[]{&GenerRandom}
    template
}*/

int FindMax(const vector<int> &a){
  int max=a[0];
  for (size_t i=1;i<a.size();i++) if (a[i]>max) max=a[i];
  return max;
}

bool CmpMax(const int &a){
    return a==1;
}

void write(const vector<vector<int> > &M){
for (size_t i=1;i<M.size();i++){
        for (size_t j=1;j<M[i].size();j++){
            cout<<M[i][j]<<' ';
        }
        cout<<'\n';
    }
}

struct Cmp{
  int Value; size_t Num,Found;

  Cmp(int v,size_t k):Value(v),Num(k),Found(0)
  {}

  bool operator()(int a){
    if (a==Value){
      if (++Found==Num) return true;
    }
  return false;
  }
};


int main()
{
    vector<int>  M(10);
    srand(time(NULL));

    for (size_t i=0;i<M.size();i++){
      M[i]=rand() %10 + 0; cout<<M[i]<<' ';
    }
    cout<<'\n';

    vector<int>::iterator it=find_if(M.begin(),M.end(),Cmp(1,2)); //Выводит номер 2-ой единички в массиве
    if (it==M.end()) cout<<"NO"; else cout<<it-M.begin(); // Если есть, то ее номер

    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//Написать функтор, который ищет строку матрицы с суммой эл-ов с n по m равной S
//Написать функтор, ищущий равные строки
//Использовать find_if
