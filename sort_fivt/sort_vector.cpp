#include <iostream>
#include <fstream>
#include "vector"
#include <cstdlib>
#include<ctime>
////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Компараторы
////////////////////////////////////////////////////////////////////

template  <typename T>
bool more(const T &a, const T &b){return a>b;}//Зачем здесь & (a>b по возрастанию, a<b по убыванию)
template  <typename T>
bool less(const T &a, const T &b){return a<b;}

////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Быстрая сортировка
////////////////////////////////////////////////////////////////////

template  <typename T, typename Cmp>
void qSort(T *a,size_t l, size_t r, Cmp cmp){
  size_t i=l,j=r; T y=a[(i+j)/2];
  do {
      while (cmp(y,a[i])) ++i;
      while (cmp(a[j],y)) --j;
      if (i<=j) {
          std::swap(a[i],a[j]); ++i; --j;
      }
  }
  while (i<=j);
  if (i<r) qSort(a,i,r,cmp);
  if (j>l) qSort(a,l,j,cmp);
}

template  <typename T, typename Cmp>
void qSort(T *a,size_t n, Cmp cmp){
    if (n!=0) qSort(a,1,n,cmp);
}

////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Пирамидальная сортировка
////////////////////////////////////////////////////////////////////

template  <typename T, typename Cmp>
T max(T a, T b, Cmp cmp){
    if (cmp(a,b)) return a; else return b;
}

template  <typename T, typename Cmp>
void siftUp(T *a, size_t j,Cmp cmp){
    while ((j>1) and (cmp(a[j],a[j/2]))) {
        std::swap(a[j/2],a[j]); j=j/2;
    }
}

template  <typename T, typename Cmp>
void siftDown(T *a, size_t n, size_t j, size_t k, Cmp cmp){
    while (k+1>2*j) { //Выполнять пока потомки существуют
        if (2*j+1>k) { //Если потомок один (значит потомок - это посл-ий элемент)
            if (cmp(a[2*j],a[j])) { //И потомок больше
                std::swap(a[j],a[2*j]);
            }
            break;
        } else {
            if (cmp(max(a[2*j],a[2*j+1],cmp),a[j])) {//Родитель не максимальный
                if (cmp(a[2*j],a[2*j+1])) {std::swap(a[j],a[2*j]); j=2*j;}//Меняем родителя с максимальным потомком
                    else {std::swap(a[j],a[2*j+1]); j=2*j+1;}
            } else break;
        }
    }
}

template  <typename T>
void siftDown(T *a, size_t n, size_t j){
    siftDown(a,n,j,n,&more<T>);
}

template  <typename T, typename Cmp>
void hSort(T *a,size_t n,Cmp cmp){
    size_t k=n;
    for (size_t i=2;i<=n;++i) siftUp(a,i,cmp);//Создаем кучу
    for (size_t i=1;i<=n-1;++i) { //Hsortим
        std::swap(a[1],a[k]); --k;
        siftDown(a,n,1,k,cmp);
    }
}

template  <typename T>
void hSort(T *a,size_t n){
    if (n!=0) hSort(a,n,&more<T>);
}
////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Сортировка пузырьком
////////////////////////////////////////////////////////////////////

template  <typename T, typename Cmp>
void bubbleSort(T *data,size_t cut,Cmp cmp){
  for (size_t i=1;i<=cut;++i)
    for (size_t j=1;j<=cut-1;++j)
      if (cmp(data[j],data[j+1])) std::swap(data[j],data[j+1]);
}

template  <typename T>
void bubbleSort(T *data,size_t cut) {
    if (cut!=0) bubbleSort(data, cut, &more<T>);
}

////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Сортировка выбором
////////////////////////////////////////////////////////////////////

template  <typename T, typename Cmp>
void selectSort(T *data,size_t cut,Cmp cmp){
  size_t min;
  for (size_t i=1;i<=cut;++i){
    min=i;
    for (size_t j=i;j<=cut;++j)
      if (cmp(data[min],data[j])) min=j;
    std::swap(data[min],data[i]);
  }
}

////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Проверка на sort
////////////////////////////////////////////////////////////////////
std::ofstream f2;

template  <typename T, typename Cmp>
bool ascORdes(std::vector <T> &a, Cmp cmp){
    size_t n = a.size()-1;
    for (size_t i=1;i<n;++i) if (cmp(a[i],a[i+1])) return 0;
    return 1;
}

template  <typename T, typename Cmp>
void Check(std::vector <T> &a,Cmp cmp){
    if (a.size()>1) f2<<"\nПоследовательность отсортирована согласно компаратору: "<<ascORdes(a, cmp)<<"\n\n";
}

////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Считывание и вывод
////////////////////////////////////////////////////////////////////

template  <typename T>
void Write(std::vector <T> &a){
    size_t n=a.size()-1;
    if (n<=0) f2<<"Массив пуст\n\n"; else {
        for (size_t i=1;i<=n;++i) f2<<a[i]<<' ';
    }
}
/*
template  <typename T>
void read(std::vector <T> &a){
    //std::ifstream f; f.open("input");
    T b; size_t n; f1>>n; a.push_back(0);
    f2<<"in : ";
    for (size_t i=1;i<=n;++i) {
        f1>>b;
        f2<<b<<' ';
        a.push_back(b);
    }
    f2<<"\nout: ";
    //return a;
}*/

////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Прослойка, чтобы не переписывать сортировки,
//////////////////////////////////////////////////////////////////// оставшиеся после статических массивов

template <typename T, typename Cmp>
void sortSelect(std::vector<T> &v, Cmp cmp){
    if (v.size()>1) selectSort(&v[0], v.size()-1, cmp);
}
template <typename T, typename Cmp>
void sortBubble(std::vector<T> &v, Cmp cmp){
    if (v.size()>1) bubbleSort(&v[0], v.size()-1, cmp);
}
template <typename T, typename Cmp>
void sortHeap(std::vector<T> &v, Cmp cmp){
    if (v.size()>1) hSort(&v[0], v.size()-1, cmp);
}
template <typename T, typename Cmp>
void sortQuick(std::vector<T> &v, Cmp cmp){
    if (v.size()>1) qSort(&v[0], v.size()-1, cmp);
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

template <typename T>
void genRandom(std::vector<T> &a){
    a.clear(); srand(time(NULL)); size_t N = 10; a.push_back(0);
    for (size_t i=1;i<=N;++i) {
        a.push_back(rand() %200 + -100);
        f2<<a[i]<<' ';
    }
    f2<<'\n';
}
template <typename T>
void genAccend(std::vector<T> &a){
    a.clear(); size_t N = 10; a.push_back(0);
    for (size_t i=1;i<=N;++i) {
        a.push_back(i);
        f2<<a[i]<<' ';
    }
    f2<<'\n';
}
template <typename T>
void genDeccend(std::vector<T> &a){
    a.clear(); srand(time(NULL)); size_t N = 10; a.push_back(0);
    for (size_t i=N;i>=1;--i) {
        a.push_back(i);
        f2<<a[i]<<' ';
    }
    f2<<'\n';
}
template <typename T>
void genEmpty(std::vector<T> &a){
    a.clear(); a.push_back(0);
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

template <typename T>
void Test(){
  typedef bool(*TComparators)(const T &a, const T &b);
  TComparators Cmp[]={&more,&less};
  typedef void(*TGenerators)(std::vector<T> &v);
  TGenerators Gen[]={&genAccend,&genDeccend,&genEmpty,&genRandom};
  typedef void(*TSortFuncs)(std::vector<T> &v, TComparators cmp);
  TSortFuncs Sort[]={&sortSelect,&sortBubble,&sortHeap,&sortQuick};
  std::vector<T> v;

   for (size_t i=0;i<4;++i) { // различные методы сортировок
       for (size_t k=0;k<2;++k) {// различные направления сортировок
           for (size_t j=0;j<4;++j) { // различные варианты инициализации вектора
              (*Gen[j])(v);
              (*Sort[i])(v, Cmp[k]);
              Write(v);
              Check(v, Cmp[k]);
           }
           f2<<"/////////////////////////////////////////////////////////\n\n";
       }
   }
}
int main(){
  f2.open("all.out");
  Test<int>();
  Test<double>();
  f2.close();
  return 0;
}
