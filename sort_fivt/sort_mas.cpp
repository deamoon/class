#include <iostream>
#include <fstream>
////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Компаратор
////////////////////////////////////////////////////////////////////

template  <typename T>
bool more(const T &a, const T &b){return a>b;}//Зачем здесь & (a>b по возрастанию, a<b по убыванию)

////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Быстрая сортировка
////////////////////////////////////////////////////////////////////

template  <typename T, typename Cmp>
void qSort(T *a,size_t l, size_t r, Cmp cmp){
  size_t i=l,j=r; T y=a[(i+j)/2];
  do {
      while (cmp(y,a[i])) i++;
      while (cmp(a[j],y)) j--;
      if (i<=j) {
          std::swap(a[i],a[j]); i++; j--;
      }
  }
  while (i<=j);
  if (i<r) qSort(a,i,r,cmp);
  if (j>l) qSort(a,l,j,cmp);
}

template  <typename T>
void qSort(T *a,size_t n){
    if (n!=0) qSort(a,1,n,&more<T>);
}

////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Пирамидальная сортировка
////////////////////////////////////////////////////////////////////

template  <typename T, typename Cmp>
T max(T a, T b, Cmp cmp){
    if (cmp(a,b)) return a; else return b;
}

template  <typename T, typename Cmp>
void siftUp(T *a, size_t j,Cmp cmp){
    while ((j>1) and (cmp(a[j],a[j/2]))) {
        std::swap(a[j/2],a[j]); j=j/2;
    }
}

template  <typename T, typename Cmp>
void siftDown(T *a, size_t n, size_t j, size_t k, Cmp cmp){
    while (k+1>2*j) { //Выполнять пока потомки существуют
        if (2*j+1>k) { //Если потомок один (значит потомок - это посл-ий элемент)
            if (cmp(a[2*j],a[j])) { //И потомок больше
                std::swap(a[j],a[2*j]);
            }
            break;
        } else {
            if (cmp(max(a[2*j],a[2*j+1],cmp),a[j])) {//Родитель не максимальный
                if (cmp(a[2*j],a[2*j+1])) {std::swap(a[j],a[2*j]); j=2*j;}//Меняем родителя с максимальным потомком
                    else {std::swap(a[j],a[2*j+1]); j=2*j+1;}
            } else break;
        }
    }
}

template  <typename T>
void siftDown(T *a, size_t n, size_t j){
    siftDown(a,n,j,n,&more<T>);
}

template  <typename T, typename Cmp>
void hSort(T *a,size_t n,Cmp cmp){
    size_t k=n;
    for (size_t i=2;i<=n;i++) siftUp(a,i,cmp);//Создаем кучу
    for (size_t i=1;i<=n-1;i++) { //Hsortим
        std::swap(a[1],a[k]); k--;
        siftDown(a,n,1,k,cmp);
    }
}

template  <typename T>
void hSort(T *a,size_t n){
    if (n!=0) hSort(a,n,&more<T>);
}
////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Сортировка пузырьком
////////////////////////////////////////////////////////////////////

template  <typename T, typename Cmp>
void bubbleSort(T *data,size_t cut,Cmp cmp){
  for (size_t i=1;i<=cut;i++)
    for (size_t j=1;j<=cut-1;j++)
      if (cmp(data[j],data[j+1])) std::swap(data[j],data[j+1]);
}

template  <typename T>
void bubbleSort(T *data,size_t cut) {
    if (cut!=0) bubbleSort(data, cut, &more<T>);
}

////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Проверка на sort
////////////////////////////////////////////////////////////////////

template  <typename T, typename Cmp>
bool ascORdes(T *a,size_t n, Cmp cmp){
    for (size_t i=1;i<n;i++) if (cmp(a[i],a[i+1])) return 0;
    return 1;
}

template  <typename T>
bool ascORdes(T *a,size_t n){
    return ascORdes(a,n,&more<T>);
}

////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// Считывание и вывод
////////////////////////////////////////////////////////////////////
std::ifstream f1; std::ofstream f2;

template  <typename T>
void write(T *a,size_t n){
    if (n==0) f2<<"Массив пуст\n\n"; else {
        for (size_t i=1;i<=n;i++) f2<<a[i]<<' ';
        f2<<"\nПоследовательность отсортирована согласно компаратору: "<<ascORdes(a,n)<<"\n\n";
    }
}

void readn(size_t *n){
    f1>>*n;
}

template  <typename T>
void read(T *a,size_t n){
    f2<<"in : ";
    for (size_t i=1;i<=n;i++) {
        f1>>a[i];//Считываем массив
        f2<<a[i]<<' ';
    }
    f2<<'\n'; f2<<"out: ";
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

int main()
{
    double *a; size_t n,k;
    f1.open("all.in"); f2.open("all.out"); f1>>k;
    for (size_t i=1;i<=k;i++){
        readn(&n); a=new double[n+1]; read(a,n);

        //bubbleSort(a,n);
        hSort(a,n);
        //qSort(a,n);

        write(a,n);
        delete [] (a);
    }
    f1.close(); f2.close();
    return 0;
}

