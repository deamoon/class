// INTERFACE /////////////////////////////////////
#include <algorithm>
#include <functional>
#include <iostream>
#include <list>
#include <memory>
#include <stdexcept>
#include <utility>
#include <vector>

/*
 * Мы реализуем стандартный класс для хранения кучи с возможностью удаления
 * элемента по индексу. Для оповещения элементов об их текущих значениях
 * индексов мы используем функцию index_change_observer.
 */

template <class T, class Compare = std::less<T>>
class MaxHeap {
 public:
  using IndexChangeObserver =
      std::function<void(const T& element, size_t new_element_index)>;

  static constexpr size_t kNullIndex = static_cast<size_t>(-1);

  explicit MaxHeap(
      Compare compare = Compare(),
      IndexChangeObserver index_change_observer = IndexChangeObserver());

  size_t Push(const T& value);
  void Erase(size_t index);
  const T& top() const;
  void Pop();
  size_t size() const;
  bool empty() const;

 private:
  IndexChangeObserver index_change_observer_;
  Compare compare_;
  std::vector<T> elements_;

  size_t parent(size_t index) const;
  size_t left_son(size_t index) const;
  size_t right_son(size_t index) const;

  bool CompareElements(size_t first_index, size_t second_index) const;
  void NotifyIndexChange(const T& element, size_t new_element_index);
  void SwapElements(size_t first_index, size_t second_index);
  size_t SiftUp(size_t index);
  void SiftDown(size_t index);
};

struct MemorySegment {
  size_t left;
  size_t right;
  size_t heap_index;

  MemorySegment(size_t left, size_t right);
  size_t size() const;
  MemorySegment Unite(const MemorySegment& other) const;
};

using MemorySegmentIterator = std::list<MemorySegment>::iterator;
using MemorySegmentConstIterator = std::list<MemorySegment>::const_iterator;

struct MemorySegmentSizeCompare {
  bool operator()(MemorySegmentIterator first,
                  MemorySegmentIterator second) const;
};

using MemorySegmentHeap =
    MaxHeap<MemorySegmentIterator, MemorySegmentSizeCompare>;

struct MemorySegmentsHeapObserver {
  void operator()(const MemorySegmentIterator& segment, size_t new_index) const;
};

/*
 * Мы храним сегменты в виде двусвязного списка (std::list).
 * Быстрый доступ к самому левому из наидлиннейших свободных отрезков
 * осуществляется с помощью кучи, в которой (во избежание дублирования
 * отрезков в памяти) хранятся итераторы на список — std::list::iterator.
 * Чтобы быстро определять местоположение сегмента в куче для его изменения,
 * мы внутри сегмента в списке храним heap_index, актуальность которого
 * поддерживаем с помощью index_change_observer. Мы не храним отдельной метки
 * для маркировки занятых сегментов: вместо этого мы кладём в heap_index
 * специальный kNullIndex. Более того, мы скрываем истинный тип
 * MemorySegmentIterator за названием SegmentHandle. Таким образом,
 * пользовательский
 * код абсолютно не зависит того, что мы храним сегменты в списке и в куче,
 * что позволяет нам легко поменять реализацию класса.
 */

class MemoryManager {
 public:
  using SegmentHandle = MemorySegmentIterator;

  explicit MemoryManager(size_t memory_size);
  SegmentHandle Allocate(size_t size);
  void Free(SegmentHandle segment_handle);
  SegmentHandle undefined_handle();

 private:
  MemorySegmentHeap free_memory_segments_;
  std::list<MemorySegment> memory_segments_;

  void AppendIfFree(SegmentHandle remaining, SegmentHandle appending);
};

size_t ReadMemorySize(std::istream& stream = std::cin);

struct AllocationQuery {
  size_t allocation_size;
};

struct FreeQuery {
  size_t allocation_query_index;
};

/*
 * Для хранения запросов используется специальный класс-обёртка
 * MemoryManagerQuery. Фишка данной реализации в том, что мы можем удобно
 * положить в него любой запрос, при этом у нас есть методы, которые позволят
 * гарантированно правильно проинтерпретировать его содержимое. При реализации
 * нужно воспользоваться тем фактом, что dynamic_cast возвращает nullptr
 * при неудачном приведении указателей.
 */

class MemoryManagerQuery {
 public:
  explicit MemoryManagerQuery(AllocationQuery allocation_query);
  explicit MemoryManagerQuery(FreeQuery free_query);

  const AllocationQuery* AsAllocationQuery() const;
  const FreeQuery* AsFreeQuery() const;

 private:
  class AbstractQuery {
   public:
    virtual ~AbstractQuery() {}

   protected:
    AbstractQuery() {}
  };

  template <typename T>
  struct ConcreteQuery : public AbstractQuery {
    T body;

    explicit ConcreteQuery(T _body) : body(std::move(_body)) {}
  };

  std::unique_ptr<AbstractQuery> query_;
};

std::vector<MemoryManagerQuery> ReadMemoryManagerQueries(
    std::istream& stream = std::cin);

struct MemoryManagerAllocationResponse {
  bool success;
  size_t position;
};

MemoryManagerAllocationResponse MakeSuccessfulAllocation(size_t position);

MemoryManagerAllocationResponse MakeFailedAllocation();

std::vector<MemoryManagerAllocationResponse> RunMemoryManager(
    size_t memory_size, const std::vector<MemoryManagerQuery>& queries);

void OutputMemoryManagerResponses(
    const std::vector<MemoryManagerAllocationResponse>& responses,
    std::ostream& ostream = std::cout);

int main() {
  std::istream& input_stream = std::cin;
  std::ostream& output_stream = std::cout;

  const size_t memory_size = ReadMemorySize(input_stream);
  const std::vector<MemoryManagerQuery> queries =
      ReadMemoryManagerQueries(input_stream);

  const std::vector<MemoryManagerAllocationResponse> responses =
      RunMemoryManager(memory_size, queries);

  OutputMemoryManagerResponses(responses, output_stream);

  return 0;
}
