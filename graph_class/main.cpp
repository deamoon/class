#include <iostream>
#include <list>
#include <vector>
using namespace std;

class MListGraph{ // Матрица смежности
  private:
    size_t VertexCount; // кол-во вершин
    vector< vector< double > > Vertex; // матрица смежности

  public:
    MListGraph(size_t n){
      VertexCount = n; Vertex.resize(n);
      for(size_t i=0; i<n; ++i) Vertex[i].resize(n);
    }

    void addVertex(size_t a, size_t b, double w){
      Vertex[a][b] = w;
    }

    size_t GetVertexCount(){
      return VertexCount;
    }

    class TVertexIterator{
      friend class MListGraph;
      public:
        vector<double>::iterator Current, End;
        size_t VertexCount;
        TVertexIterator(vector<double>::iterator it_begin, vector<double>::iterator it_end,size_t n):VertexCount(n){
          Current = it_begin; End = it_end;
        }
        double Weight(){
          return *Current;
        }
        size_t Index(){
          return (VertexCount-(End-Current));
        }
        bool IsValid(){
          return Current!=End;
        }
        TVertexIterator & operator ++ (){
          ++Current;
          return *this;
        }
    };

    TVertexIterator GetConnected(size_t k){ // итератор на вершиины связынный с этой
      return TVertexIterator(Vertex[k].begin(), Vertex[k].end(), VertexCount);
    }

};

struct TEdge{
  size_t V; // номер
  double W; // вес
};

class SListGraph{ // Список смежности
  private:
    size_t VertexCount; // кол-во вершин
    vector< list< TEdge > > Vertex; // список смежности

  public:
    SListGraph(size_t n){
      VertexCount = n; Vertex.resize(n);
    }

    void addVertex(size_t a, size_t b, double w){
      TEdge s; s.V = b; s.W = w;
      Vertex[a].push_back(s);
    }

    void outVertex(size_t a){
      cout<<Vertex[a].back().W;
    }

    size_t GetVertexCount(){
      return VertexCount;
    }

    class TVertexIterator{
      friend class SListGraph;
      public:
        list<TEdge>::iterator Current, End;
        TVertexIterator(list<TEdge>::iterator it_begin, list<TEdge>::iterator it_end){
          Current = it_begin; End = it_end;
        }
        double Weight(){
          return (*Current).W;
        }
        size_t Index(){
          return (*Current).V;
        }
        bool IsValid(){
          return Current!=End;
        }
        TVertexIterator & operator ++ (){
          ++Current;
          return *this;
        }
    };

    TVertexIterator GetConnected(size_t k){ // итератор на вершиины связынный с этой
      return TVertexIterator(Vertex[k].begin(), Vertex[k].end());
    }

};

template<typename TGraph>
class TDeikstra{
  private:
    vector<bool> Fixed; // Посещенность городов
    vector<double> Weights; // Итоговые расстояния
    double Max_NUM;
  public:
    TDeikstra(TGraph &Graph){
      Max_NUM = 2*1000*1000*1000;
      for(size_t i=0;i<Graph.GetVertexCount();++i){
        Weights.push_back(Max_NUM);
        Fixed.push_back(1);
      }
    }
    int FindMin(){
      double min = Max_NUM;
      size_t res;
      for(size_t i=0;i < Weights.size();++i){
        if ((Weights[i] < min) && (Fixed[i])) {
          res = i;
          min = Weights[i];
        }
      }
      if (min == Max_NUM) return -1; else {
        Fixed[res] = 0;
        return res;
      }
    }
    void Correct(int k1, TGraph &Graph){
      size_t k = k1;
      for(auto it = Graph.GetConnected(k); it.IsValid(); ++it){
        if (it.Weight() + Weights[k] < Weights[it.Index()]){
          Weights[it.Index()] = it.Weight() + Weights[k];
        }
      }
    }
    void Do(size_t start, TGraph &Graph){
      int k;
      Weights[start]=0;
      while (true){
        k=FindMin();
        if (k==-1) break;
        Correct(k,Graph);
      }
    }
    void GetResult(){
      for(size_t i=0;i<Weights.size();++i){
        if (Weights[i] == Max_NUM) cout<<"-1 "; else cout<<Weights[i]<<' ';
      }
    }
    void GetResultNum(int k){
      if (Weights[k] == Max_NUM) cout<<"-1"; else cout<<Weights[k];
    }
};

/*void Correct(int k){
  for(auto it = g.GetConnected(k); it.IsValid(); ++it){
    if (it.Weight() + D[k] < D[i]){ // i - it.Index

    }
  }
}*/

int main()
{
    // Алгоритм проверен
    // http://informatics.mccme.ru/moodle/mod/statements/view.php?id=193#1
    // Меняем MListGraph на SListGraph и понимаем, что все работает
    freopen("input.txt","r",stdin); //freopen("output.txt","w",stdout);
    size_t n,a,b,m,start; double w;
    cin>>n>>m; MListGraph G(n); // n - кол-во вершин, m - кол-во запросов
    for(size_t i=0;i<m;++i){
      cin>>a>>b>>w;
      G.addVertex(a,b,w); // Неориентированный
      G.addVertex(b,a,w);
    }

    cin>>start;
    TDeikstra<MListGraph> alg(G);
    alg.Do(start,G);
    alg.GetResult();
}
