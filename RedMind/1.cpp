#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
void Test(int a[100]) {
	cout<<sizeof(a)/sizeof(a[0])<<endl;
	int *b = new int[100];
	cout<<sizeof(b)/sizeof(b[0])<<endl;
	int c[100];
	cout<<sizeof(c)/sizeof(c[0])<<endl;
}
/*
class A {
  public:
    A(int dataSize)
      : data_(new int[dataSize]), dataSize_(dataSize)
    {}
    ~A()
    {
      delete [] data_;
    }
    A(const A & rgt) {
    	dataSize_ = rgt.dataSize_;
    	data_ = new int[dataSize_];
    	copy(rgt.data_, rgt.data_ + dataSize_, data_);
    }
    void Swap(A & tmp) {
    	swap(tmp.data_, data_);
    	swap(tmp.dataSize_, dataSize_);
    }
    A & operator = (const A & b) {
    	A tmp(b);
    	Swap(tmp);
    	return *this;
    }
    void p() {
    	cout<<dataSize_<<endl;
    }
  private:
    int *data_;
    int dataSize_;
};
*/
/*struct A
{
  A() {std::cout << "CA";}
  ~A() {std::cout << "DA";}
};
struct B : public A
{
  B() {std::cout << "CB";}
  B(const B &) {std::cout << "CCB";}
  ~B() {std::cout << "DB";}
};
int main()
{
  B b;
  A *pa = &b;
  A a(*pa);
}*/
/*
 struct A
 {
   A() {std::cout << "C";}
   ~A() {std::cout << "D";}
 };
 int main()
 {
   A *pa = new A[5];
   delete[] pa;
}*/
/*
int main() {
  std::vector<int> v;
  v.push_back(1);v.push_back(2);v.push_back(3);
  for (std::vector<int>::const_iterator it = v.begin(), end = v.end(); it != end; ++it)
  {
    if (*it % 2 == 0)
      v.push_back(++*it);
  }
  return 0;
} 
*/
/*
int main(int argc, char const *argv[])
{
	for (int i = 1; i <= 100; ++i)
	{		
		cout<<i;
		if (i%3==0) cout<<" buzz"; else
		  if (i%5==0) cout<<" fizz"; else
		    if (i%15==0) cout<<" fizzbuzz";
		cout<<endl;    
	}
	return 0;
}*/
	/*
#include <stdio.h>
int main(int argc, char const *argv[])
{
	const char *str = "hello";
	char *str1 = const_cast<char*>(str);
	str1[0] = 'H';

	int i;
	const int * p1 = &i;
	int* p2 = const_cast<int *> (p1);
	*p2 = 42;

	class Base { public: virtual ~Base(void) { } };
	class Derived1 : public Base {public: virtual ~Derived1(void) { } };
	class Derived2 : public Base {public: virtual ~Derived2(void) { } };
	class Unrelated { public: virtual ~Unrelated(void) { } };
	Base* pb = new Derived1;
	Derived1 * pd1 = dynamic_cast<Derived1 *>(pb);
	Derived2 * pd2 = dynamic_cast<Derived2 *>(pb);
	Unrelated* pu1 = dynamic_cast<Unrelated*>(pb);
	Unrelated* pu2 = reinterpret_cast<Unrelated*>(pb);
	printf("%p %p %p %p %p\n", pb, pd1, pd2, pu1, pu2);
	// к чему приведет попытка вызвать delete от каждого из 5 указателей?
	return 0;
}*/


#include <iostream>
class shoes {
  public:
    virtual void show() { std::cout << "shoes"; }
};
class redshoes: public shoes {
  public:
    virtual void show() { std::cout << "redshoes"; }
};
class box {
  public:
    void show(shoes* p)    { std::cout << " box("; p->show(); std::cout << ")\n"; }
    void show(redshoes* p) { std::cout << " redbox("; p->show(); std::cout << ")\n"; }
};
int main() {
  shoes* pa = new redshoes();
  box* pb = new box();
  pa->show();
  pb->show(pa);
}






/*
int main() {
	A a(5);
	A b(6);
	a = b;
	a.p();
	//int c[200];
	//Test(c);
}*/