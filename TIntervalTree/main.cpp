#include <iostream>
#include <vector>
#include <math.h>
#include <cstdlib>
#include <limits>
#include <ctime>
using namespace std;
typedef int(*TComparators)(const int &a, const int &b);

template  <typename T>
T sum(const T &a, const T &b){return a+b;}
template  <typename T>
T min(const T &a, const T &b){if (a<b) return a; else return b;}
template  <typename T>
T max(const T &a, const T &b){if (a>b) return a; else return b;}

template  <typename T>
struct TTraits{
  TComparators f;
  T Zero;
  TTraits(TComparators f1, T z){
    f=f1; Zero=z;
  }
};

//1. Должен параметризоваться типом элементов и типом целевой функции (max/min/sum).
//2. Должен быть конструктор (параметризуется макс. глубиной и целевой функцией),
//деструктор, конструктор копирования, оператор присваивания.
//3. Должны быть методы: Modify(idx, value), Calc(from, to).
//4. Тесты: глубина 1, вставка во все элементы, запросы всех подмножеств (сравнение с прямым подсчетом).
template  <typename T>
class TIntervalTree{
  private:
    T *tree;
    size_t size;
    TComparators cmp;
    T Zero;
  public:
    template <typename S>
    TIntervalTree(size_t n, T *a, S mono){ //Конструктор, принимающий массив
      size=1; cmp=mono.f; Zero=mono.Zero;
      while (size<n) size*=2;
      tree = new T[2*size-1];
      for(size_t i=0;i<n;++i) tree[i+size-1]=a[i];
      for(size_t i=n;i<size;++i) tree[i+size-1] = Zero;
      for(int i=size-2;i>=0;--i) {
        tree[i]=cmp(tree[2*i+1],tree[2*i+2]);
      }
    }
    template <typename S>
    TIntervalTree(size_t height, S mono){ //Конструктор, принимающий только максимальную глубину
      size=pow(2,height-1); cmp=mono.f; Zero=mono.Zero;
      size_t n=2*size-1;
      tree = new T[n];
      for(size_t i=n;i<size;++i) tree[i+size-1] = Zero;
    }
    TIntervalTree (const TIntervalTree& t) { // Конструктор копирования
        size_t n = (t.size)*2-1;
        T *a = new T[n];
        for (size_t i=0;i<n;++i) a[i]=t.tree[i];
        size=t.size;  tree=a; cmp=t.cmp; Zero=t.Zero;
    }
    void Swap(TIntervalTree &a){
      swap(tree,a.tree); swap(size,a.size); swap(cmp,a.cmp); swap(Zero,a.Zero);
    }
    TIntervalTree &operator =(const TIntervalTree &a){ // Оператор присваивания
      TIntervalTree tmp(a);
      Swap(tmp);
      return *this;
    }
    void write(){
      for(size_t i=0;i<2*size-1;++i) cout<<tree[i]<<' ';
      cout<<'\n';
    }
    void write(size_t l, size_t r){
      for(size_t i=l;i<=r;++i) cout<<tree[i]<<' ';
      cout<<'\n';
    }
    void modify(size_t n, T x){
      int t = n + size-1;
      tree[t]=x;
      while (t>=1) {
        t=(t-1)/2; tree[t]=cmp(tree[2*t+1],tree[2*t+2]);
      }
    }
    int calc(int l, int r){
      l+=size-1; r+=size-1;
      int res = Zero;
      while (l<=r){
        if (l%2==0) { res=cmp(res,tree[l]); l=l/2; } else l=l/2;
        if (r%2==1) { res=cmp(res,tree[r]); if (r!=1) r=(r-2)/2; else r=-1; } else r=(r-2)/2;
      }
      return res;
    }
    ~TIntervalTree(){
      delete[] tree;
    }
    /*int RSQ(int i, int j, int v, int l, int r){if ((j<l)||(r<i)) return 0;if ((i<=l)&&(r<=j)) return a[v];
      int m=(l+r)/2;return RSQ(i,j,2*v,l,m)+RSQ(i,j,2*v+1,m+1,r);}*/
};

int main()
{
  TTraits<int> Sum(&sum,0);
  TTraits<int> Max(&max,numeric_limits<int>::min());
  TTraits<int> Min(&min,numeric_limits<int>::max());

  //Проверка конструктора от массива
    int a[13]; for(size_t i=1;i<=13;++i) a[i-1]=i;
    TIntervalTree<int> t1(13,a,Sum);
    TIntervalTree<int> t2(13,a,Max);
    TIntervalTree<int> t3(13,a,Min);
    t1.write();t2.write();t3.write(); cout<<'\n';
  //Проверка конструктора от массива

  //Глубина 1
    TIntervalTree<int> w(1,Max);
    w.modify(0,3);
    w.write(); cout<<'\n';
  //Глубина 1

  //Вставка во все элементы
    TIntervalTree<int> g(11,Sum);
    srand(time(NULL));
    for(size_t i=0;i<1023;++i) {
      g.modify(i,rand() %20 + -10);
    }
    g.write(1,15); cout<<'\n';
  //Вставка во все элементы

  //Запрос всех подмножеств (для тестирования нужно изменить 3-й аргумент)
    TTraits<int> cmp = Min; int res = cmp.Zero;
    TIntervalTree<int> q(6,cmp);
    int b[32];
    int r;
    for(size_t i=0;i<28;++i) {
      r = rand() %20 + -10;
      b[i]=r; q.modify(i,r);
    }
    q.write();
    for(size_t i=0;i<=27;++i){
      for(size_t j=i;j<=27;++j){
        res = cmp.Zero;
        for(size_t k=i;k<=j;++k) res=cmp.f(res,b[k]);
        if (res!=q.calc(i,j)) cout<<res<<' '<<q.calc(i,j)<<' '<<i<<' '<<j<<' '<<" Very Big Problem\n";
      }
    }
  //Запрос всех подмножеств

  return 0;
}
