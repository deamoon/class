#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define max 10005

// функция произведения a[0]  и  b[0] длиных чисел. числа представлены в обратном порядке.
// с - ответ , p - массив произведений b на 0..9,память динамическая (иначе обнылить c и p)
int* pro(int *a, int *b);
int* pro(int *a, int *b)
{
    int ost = 0;
    int i, j, **p,*c;
    c = (int*)calloc(a[0]+b[0]+3,sizeof(int));
   if (a[a[0]]!=0 && b[b[0]] !=0)
   {
    p = (int**)malloc(10*sizeof(int*));
    for (i = 0;i < 10 ;i++)
          p[i] = (int*)calloc(max,sizeof(int));
    for (i = 1; i < 10; i++)
     {  for (j = 1; j <= p[i-1][0] || j <= b[0]||ost ;j++)
         {
             p[i][j] = p[i-1][j]+b[j] + ost;
             ost = p[i][j] > 9;
             if (ost)
               p[i][j]-=10;
         }
         p[i][0] = j-1;
    }
    ost =0;
    for (i = 1; i <= a[0];i++)
      {
          for (j = 1; j <= p[a[i]][0]||ost;j++)
          {
              c[i+j -1] += p[a[i]][j] + ost ;
               ost = c[i+j -1] > 9;
               if (ost)
                 c[i+j-1] -= 10;
          }
      }
      c[0] = i+j - 3;
      for (i = 0; i< 10; i++)
      free(p[i]);
      free(p);
   } else
   {
       c[0] = 1;
       c[1] = 0;
   }
   free(b);

    return c;
}
int* per(int *a, FILE *in, int len);
int* per(int *a, FILE *in, int len)
{
    int i;
    char c;
    for (i = len; i >= 1; i--)
    {
        c = fgetc(in);
        a[i] = c - '0';
    }
    return a;
}
int main(void)
{
    int i,*a,*b, len1 = 0, len2 = 0;
    FILE *in1, *in2, *out;
    char c;
    a = (int* )malloc(max*sizeof(int));
    b = (int* )malloc(max*sizeof(int));
    out=fopen("output.txt","w");
    if(!out) return -1;
    in1=fopen("in1put.txt","r");
    if(!in1) {fprintf (out,"error"); fclose(out);return -1;}
    in2=fopen("in2put.txt","r");
    if(!in2) {fprintf (out,"error"); fclose(out);return -1;}
    while (fscanf(in1, "%c", &c) == 1) {len1++;}
    rewind(in1);
    while (fscanf(in2, "%c", &c) == 1){len2++;}
    rewind(in2);
    a[0] = len1;
    b[0] = len2;
    a=per(a, in1, len1);
    b=per(b, in2, len2);
    a = pro(a,b);
    for(i = a[0] ; i > 0; i--)
      fprintf(out,"%d", a[i]);
      free(a);
   return 0;
}
