#include <iostream>
#include <limits>
using namespace std;

template  <typename T>
struct TSumTraits{
  T operator()(const T &a, T &b){
    return a+b;
  }
  T Zero()const{
    return T();
  }
};
template  <typename T>
struct TMaxTraits{
  T operator()(const T &a, T &b){
    if (a>b) return a; else return b;
  }
  T Zero()const{
    return numeric_limits<T>::Min(); //type_traits//return -INF;
  }
};

typedef int(*TComparators)(const int &a, const int &b);
template  <typename T>
T sum(const T &a, const T &b){return a+b;}

//typedef (*TMonoid);
int main()
{
    TSumTraits<int> Sum; TMaxTraits<int> Max;
    //TMonoid Mono[]={&Sum,&Max};
    TComparators Cmp[]={&sum};
    //wr(1);
    //Tree t(5,6);
    cout<<sum(1,2)<<'\n';
    //TComparators Cmp[]={&sum};
    cout << "Hello world!" << endl;
    return 0;
}
