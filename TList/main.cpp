#include <iostream>
#include <math.h>
using namespace std;

class TstaticArray{
  private:
    size_t Size;
    int *Data;
  public:
    TstaticArray(size_t sz):Size(sz){
      Data = new int[Size];
      for (size_t i=0;i<Size;++i) Data[i]=0;
    }
    TstaticArray (const TstaticArray& t) {
        int *a = new int[t.Size];
        for (size_t i=0;i<t.Size;++i) a[i]=*(t.Data+i);
        Size=t.Size;  Data=a;
    }
    void print(){
        for (size_t i=0;i<Size;++i) cout<<*(Data+i)<<' '; cout<<"\n\n";
    }
    void feel(){
        for (size_t i=0;i<Size;++i) *(Data+i)=i;
    }
    void feel1(){
        for (size_t i=0;i<Size;++i) *(Data+i)=i+1;
    }
    TstaticArray &operator =(const TstaticArray &l){ //Не самый лучший способ
        if (this!=&l){ //проверка на случай a=a; delete[] Data; Data=NULL; Data = new int[l.Size]; } }*/
    void Swap(TstaticArray &a){
      swap(Size,a.Size);
      swap(Data,a.Data);
    }
    TstaticArray &operator =(const TstaticArray &l){
      TstaticArray tmp(l);
      Swap(tmp);
      return *this;
    }        //Саттер решение сложных задач на С++

    ~TstaticArray(){
        delete[] Data;
    }
};

template<typename T>
class TList{
    private:
    ////////////////////////////////////////
    struct TElem{
        T Data;
        TElem *Next;
        TElem *Prev;
    };
    ///////////////////////////////////////
    TElem *Front;
    TElem *Last;
    int razmer;
    ///////////////////////////////////////
    public:

    TList (){
        Front=NULL; Last=NULL; razmer=0;
    }

    class TIterator{
        friend class TList;
        TElem *Ptr;
        public:
        T & operator * (){
            return Ptr->Data;
        }
        TIterator & operator ++ (){
            Ptr=Ptr->Next;
            return *this;
        }
        TIterator & operator -- (){
            Ptr=Ptr->Prev;
            return *this;
        }
        bool operator == (const TIterator &a) const{
            return Ptr==a.Ptr;
        }
        TIterator (TElem *p)
            :Ptr(p){
        }
    };

    void print(){
        TElem *t=Front;
        if (t==NULL) cout<<"Clear";
        while (t!=NULL) {
            cout<<t->Data<<' ';
            t=t->Next;
        }
        cout<<'\n';
    }

    void insert(TIterator &it, const T &data){
        if ((it.Ptr==NULL)&&(Front!=NULL)) {
            TElem *pr = new TElem; ++razmer;
            pr->Data = data;
            pr->Next = NULL;
            pr->Prev = Last;
            if (Last==NULL) Front=pr; else Last->Next = pr;
            Last = pr;
            return;
        } //Когда NULL добавлять в конец или нет, решили добавлять.
        if (it.Ptr==NULL) {
            Front = new TElem; ++razmer;
            Front->Data = data;
            Front->Next = NULL;
            Front->Prev = NULL;
            Last=Front;
            return;
        }
        TElem *pr = new TElem; ++razmer;
        pr->Data = data;
        pr->Next = it.Ptr;
        pr->Prev = it.Ptr->Prev;
        if (pr->Prev==NULL) { Front=pr; it.Ptr->Prev=pr;} else {
            it.Ptr->Prev->Next = pr;
            it.Ptr->Prev = pr;
        }
        return;
    }

    void push_front(const T &data){
        TIterator P(Front);
        this->insert(P,data);
        /*TElem *pr = new TElem; ++razmer;pr->Data = data;pr->Next = Front;pr->Prev = NULL;if (Front==NULL) Last=pr; else Front->Prev = pr;Front = pr;*/
        return;
    }

    void push_back(const T &data){
        TIterator P(NULL);
        this->insert(P,data);
        /*TElem *pr = new TElem; ++razmer;pr->Data = data;pr->Next = NULL;pr->Prev = Last;if (Last==NULL) Front=pr; else Last->Next = pr;Last = pr;*/
        return;
    }

    TIterator begin(){
        TIterator p(Front);
        return p;
    }
    TIterator endl(){
        TIterator p(Last);
        return p;
    }
    TIterator end(){
        TIterator p(NULL);
        return p;
    }
    int size(){
        return razmer;
    }
    void pop_back(){
        if (razmer==0) return;
        --razmer;
        if (razmer==0) {Last=NULL; Front=NULL; return;}
        TElem *tmp = Last->Prev;
        delete Last;
        Last=tmp;
        Last->Next = NULL;
    }
    void pop_front(){
        if (razmer==0) return;
        --razmer;
        if (razmer==0) {Last=NULL; Front=NULL; return;}
        TElem *tmp = Front->Next;
        delete Front;
        Front = tmp;
        Front->Prev = NULL;
    }
    void clear(){
        TElem *t=Front;
        while (Front!=NULL) {
            t=t->Next;
            delete Front;
            Front=t;
        }
    }
/////////////////////////////////////////////////////////
    /*TList(const TList &a){
        if (a.razmer==0) {Front=NULL; Last=NULL; razmer=0; return;}
        razmer=a.razmer;
        TElem *t = new TElem[razmer];
        t->Prev=NULL;
        (t+(razmer-1))->Next=NULL;
        TElem *tmp = a.Front;
        for (int i=0;i<razmer;++i) {
            (t+i)->Data=tmp->Data;
            if (i!=0) (t+i)->Prev = t+(i-1);
            if (i!=razmer-1) (t+i)->Next = t+(i+1);
            tmp=tmp->Next;
        }
        Front=t; Last=t+(razmer-1);
        // недописано нифига
    }*/
    /*
    TList(const TList &a){
        Front=NULL; Last=NULL; razmer=0;
        TElem *tmp = a.Front;
        for (int i=0;i<a.razmer;++i) {
            this->push_back(tmp->Data);
            tmp=tmp->Next;
        }
    }
    void Swap(TList &a){
      swap(razmer,a.razmer);
      swap(Front,a.Front);
      swap(Last,a.Last);
    }
    TList &operator =(const TList &a){
      TList tmp(a);
      Swap(tmp);
      return *this;
    }*/
//////////////////////////////////////////////////////////
    ~TList(){
        this->clear();
    }
};

//push_back,push_front, size,begin,end,void insert(const ITterator &it, const T &data) (вставляем перед элементом)
//insert(iter, value), push_back(value), push_front(value), pop_back(), pop_front(), begin(), end(), size(), clear().

int main()
{
    //Проверка на конструктор копирования и оператор присваивания
    TList<int> a;
    a.push_back(4); a.push_back(5); a.push_back(6); a.push_back(7); a.push_back(8); a.push_back(9); a.push_back(10);
    TList<int> b,c(a); b=a;
    TstaticArray s(9),s1(9); s.feel();
    s1=s;
    a.push_back(11); a.push_front(3);
    a.print(); b.print(); c.print();
    for (TList<int>::TIterator i=a.endl(); !(i==a.end()); --i) cout<<*i<<' '; cout<<'\n';
    for (TList<int>::TIterator i=b.endl(); !(i==b.end()); --i) cout<<*i<<' '; cout<<'\n';
    for (TList<int>::TIterator i=c.endl(); !(i==c.end()); --i) cout<<*i<<' '; cout<<'\n';
    cout<<'\n';
    //Проверка на конструктор копирования и оператор присваивания

    //Проверка на вставку и удаление
    TList<int> d;
    d.push_back(1); d.print();
    d.push_back(2); d.print();
    d.push_back(3); d.print();
    d.push_front(0); d.print();
    d.push_back(4); d.print();
    d.push_back(5); d.print();
    d.push_front(-1); d.print();
    d.push_back(6); d.print();
    d.push_front(-2); d.print();
    cout<<"Razmer = "<<d.size()<<'\n';

    d.pop_back(); d.print();
    d.pop_front(); d.print();
    d.pop_back(); d.print();
    d.pop_back(); d.print();
    d.pop_back(); d.print();
    d.pop_front(); d.print();
    d.pop_front(); d.print();
    cout<<"Razmer = "<<d.size()<<'\n';
    cout<<'\n';
    //Проверка на вставку и удаление

    //Проверка работы итераторов
    TList<int>::TIterator it=d.begin();
    ++it; d.insert(it,9); d.print();
    ++it; d.insert(it,23); d.print();
    it=d.endl(); --it; d.insert(it,17); d.print();
    for (TList<int>::TIterator i=d.begin(); !(i==d.end()); ++i) cout<<*i<<' '; cout<<'\n';
    for (TList<int>::TIterator i=d.endl(); !(i==d.end()); --i)  cout<<*i<<' '; cout<<'\n';
    cout<<'\n';
    //Проверка работы итераторов

    //Крайние случаи
    TList<int> e;
    e.print(); e.push_back(1); e.print();

    TList<int> f;
    f.print(); f.push_front(2); f.print();

    TList<int> g;
    g.print();
    TList<int>::TIterator it2=g.begin();
    g.insert(it2,3);
    g.print();
    cout<<'\n';
    //Крайние случаи

    //Много элементов
    TList<double> h;
    for(size_t i=0;i<100; ++i) h.push_back(sqrt(i));
    h.print(); h.clear(); h.print();
    cout<<'\n';
    //Много элементов

    //Добавление в конец непустого списка
    TList<int> k;
    k.push_back(1);
    TList<int>::TIterator it3=k.end();
    k.insert(it3,2);
    k.print();
    for (TList<int>::TIterator i=k.endl(); !(i==k.end()); --i)  cout<<*i<<' '; cout<<'\n';
    //Добавление в конец непустого списка

    //Да, ok. Добавьте тестов на большое количество данных.
    //Например, прогоните 10000 раз тест по созданию списка,
    //добавлению в конец/начало, добавлению 1000 элементов.
    for (size_t i=0;i<10000;++i){
      TList<int> z1; TList<int> z2;
      for (size_t j=0;j<1000;++j){
        z1.push_back(j); z2.push_front(j);
      }
      z1.clear(); z2.clear();
    }
    TList<int> z3;
    for (size_t j=0;j<10000000;++j){
      z3.push_back(j); z3.pop_back();
    }
    //Да, ok. Добавьте тестов на большое количество данных.
    //Например, прогоните 10000 раз тест по созданию списка,
    //добавлению в конец/начало, добавлению 1000 элементов.

    //Проверка класса массива
    TstaticArray s(5); s.feel(); TstaticArray s1(10); s1.feel1(); //s.print(); s1.print();
    s1.print();
    s1=s;
    s1.print();
    //Проверка класса массива

    return 0;
}
