#include <iostream>
#include <ctime>
#include <string>
#include <cstdlib>
using namespace std;

template <typename TData>
class Treap {
private:
	struct Node {
		int size;
		int Priority;
		TData Data;
		Node *Left;
		Node *Right;
        Node(TData data) {
            size = 1;
            Priority = rand() | (rand() << 16);
            Data = data;
            Left = Right = 0;
        }
	};
	typedef Node* pNode;
	pNode Root;

	void Node_delete(pNode Now){
        if (Now == NULL) return;
        Node_delete(Now->Left);
        Node_delete(Now->Right);
        delete Now;
    }

	pNode Merge(pNode L, pNode R){
        if (L == NULL) return R;
        if (R == NULL) return L;

        if (L->Priority > R->Priority){
            pNode New = L;
            New->size += R->size;
            New->Right = Merge( L->Right, R);
            return New;
        }
        else {
            pNode New = R;
            New->size += L->size;
            New->Left = Merge(L, R->Left);
            return New;
        }
    }

	int Size(pNode Now) const{
        if (Now == NULL) return 0;
        return Now->size;
    }

    void Recalc_size(pNode Now){
        if (Now == NULL) return;
        Now->size = Size(Now->Left) + Size(Now->Right) + 1;
    }

	void Split(int div, pNode Now, pNode &L, pNode &R){
        if (Now == NULL) {
            L = NULL;
            R = NULL;
            return;
        }
        int L_size = 0;
        if (Now->Left != NULL)
            L_size = Now->Left->size;

        if (L_size + 1 <= div) {
            L = Now;
            Split(div - L_size - 1, Now->Right, L->Right, R);
        }
        else {
            R = Now;
            Split(div, Now->Left, L, R->Left);
        }
        Recalc_size(L);
        Recalc_size(R);
    }

    void Print_now(pNode p){
      if (!p) return;
      cout<<p->Data<<' ';
      if (p->Left)  Print_now(p->Left);
      if (p->Right) Print_now(p->Right);
    }

public:

    Treap(){
        Root = NULL;
    }

    pNode Ind(const int Index) {
        pNode L = NULL, R = NULL, L1 = NULL, R1 = NULL;
        Split(Index, Root, L, R);
        Split(1, R, L1, R1);
        R = Merge(L1, R1);
        Root = Merge(L, R);
        return L1;
    }
    TData operator [] (const int Index) {
        return Ind(Index)->Data;
    }

    Treap (Treap &t) {
        Root = NULL;
        for(int i=0; i < Size(t.Root); ++i){
            push_back(t[i]);
        }
    }

    void Swap(Treap &t){
        swap(Root,t.Root);
    }
    Treap &operator =(Treap &t){
        Treap tmp(t);
        Swap(tmp);
        return *this;
    }

    ~Treap(){
        Node_delete(Root);
        Root = NULL;
    }

    class Iterator{
        friend class Treap;
        pNode Ptr;
        Treap<TData> *root;
        int num;
    public:
        TData & operator * (){
            return Ptr->Data;
        }
        Iterator & operator ++ (){
            ++num;
            Ptr = root->Ind(num);
            return *this;
        }
        bool operator == (const Iterator &t) const{
            return ((num==t.num));
        }
        Iterator (pNode p, int n, Treap<TData> *r){
            Ptr = p;
            num = n;
            root = r;
        }
    };

    Iterator begin(){
        pNode p = Root;
        while (p->Left) p = p->Left;
        Iterator res(p,0,this);
        return res;
    }
    Iterator end(){
        pNode p = Root;
        while (p->Right) p = p->Right;
        Iterator res(p,Size(Root)-1,this);
        return res;
    }

	void Print(){
        Print_now(Root); cout<<'\n';
    }

	void insert(int Index, TData data){
        pNode L = NULL, R = NULL, New;
        New = new Node(data);
        if (Root != NULL) {
            Split(Index, Root, L, R);
            Root = Merge(L, New);
            Root = Merge(Root, R);
        }
        else
            Root = New;
    }

    void push_back(TData data){
        insert(Size(Root),data);
    }
    void push_front(TData data){
        insert(0,data);
    }
    int size(){
        return Size(Root);
    }
};

int main(){
	srand(time(0));
	Treap<int> Tree;

    Tree.insert(0,1);Tree.insert(1,2);Tree.insert(2,3);
    Tree.push_back(4); Tree.push_front(8); Tree.push_back(6);

    Treap<int> Tree2;
    Tree2=Tree;

    Tree.push_back(90);
    for (int i=0; i<Tree.size(); ++i) cout<<Tree[i]<<' ';
    cout<<'\n';

    for (int i=0; i<Tree2.size(); ++i) cout<<Tree2[i]<<' ';

    cout<<'\n';
    Treap<int>::Iterator it1=Tree.begin();
    Treap<int>::Iterator it2=Tree.end();
    do{
      ++it1; cout<<*it1<<' ';
    }
    while (!(it1==it2));

    for (int i=0; i<10000; ++i){
       Tree.push_back(rand() % 10000);
    }
    for (int i=0; i<20; ++i) cout<<Tree[i]<<' ';

	return 0;
}
