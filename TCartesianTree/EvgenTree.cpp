#include <iostream>
#include <ctime>
#include <string>
#include <cstdlib>
using namespace std;

template <typename TData>
class Treap {
private:
	struct Node {
		int size;
		int Priority;
		TData val;
		Node *Left;
		Node *Right;
        Node(TData newval) {
            size = 1;
            Priority = rand() | (rand() << 16);
            val = newval;
            Left = Right = 0;
        }
	};
	typedef Node* pNode;
	typedef const Node* const_pNode;
	pNode root;

	void Node_delete(pNode Now) {
	if (Now == NULL)
		return;
	Node_delete(Now->Left);
	Node_delete(Now->Right);
	delete Now;
    }

	pNode Merge(pNode L, pNode R) {
	if (L == NULL) {
		return R;
	}
	if (R == NULL) {
		return L;
	}
	if (L->Priority > R->Priority) {
		pNode New = L;
		New->size += R->size;
		New->Right = Merge( L->Right, R);
		return New;
	}
	else {
		pNode New = R;
		New->size += L->size;
		New->Left = Merge(L, R->Left);
		return New;
	}
    }

	int Size(const_pNode Now) const {
        if (Now == NULL)
            return 0;
        return Now->size;
    }

    void Recalc_size(pNode Now) {
        if (Now == NULL)
            return;
        Now->size = Size(Now->Left) + Size(Now->Right) + 1;
    }

	void Split(int div, pNode Now, pNode &L, pNode &R) {
        if (Now == NULL) {
            L = NULL;
            R = NULL;
            return;
        }
        int L_size = 0;
        if (Now->Left != NULL)
            L_size = Now->Left->size;

        if (L_size + 1 <= div) {
            L = Now;
            Split(div - L_size - 1, Now->Right, L->Right, R);
        }
        else {
            R = Now;
            Split(div, Now->Left, L, R->Left);
        }
        Recalc_size(L);
        Recalc_size(R);
    }

    void Print_now(string &ans, const_pNode Now) const {
        if (Now->Left != NULL)
            Print_now(ans, Now->Left);
        ans.push_back(Now->val);
        if (Now->Right != NULL)
            Print_now(ans, Now->Right);
    }

public:

    Treap() {
        root = NULL;
    }

    ~Treap(){
        Node_delete(root);
        root = NULL;
    }

	void Print(string &ans) const {
        if (root == NULL)
            return;
        ans.reserve(root->size);
        Print_now(ans, root);
    }

	void Add(int Index, TData val) {
        pNode L = NULL, R = NULL, New;
        New = new Node(val);
        if (root != NULL) {
            Split(Index, root, L, R);
            root = Merge(L, New);
            root = Merge(root, R);
        }
        else
            root = New;
    }

};

int main(){
	srand(time(0));
	//Treap MyTree;
	for (int i=0; i<10; ++i)
      //MyTree.Add(i, rand()%100);

	return 0;
}

