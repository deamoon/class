#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

template <typename TData>
class TTreap{
  private:
    struct TNode{
      TNode *Left;
      TNode *Right;
      int Size;
      double y; // Priority
      TData Data;
    };
    TNode *Root;

    TTreap(double y, TData data, TNode *left, TNode *right){
      this.y = y;
      this.Data = data;
      this.Left = left;
      this.Right = right;
    }

    int SizeOf(TNode *tree){
      if (tree==NULL) return 0; else return tree->Size;
    }
    void Recalc(){
      Size = SizeOf(Left) + SizeOf(Right) + 1;
    }

    void Split(int x, out ImplicitTreap L, out ImplicitTreap R){
      TNode *newTree = NULL;
      int curIndex = SizeOf(Left) + 1;

      if (curIndex <= x){
        if (Right == null)
            R = null;
        else
            Right.Split(x - curIndex, out newTree, out R);
        L = new ImplicitTreap(y, Cost, Left, newTree);
        L.Recalc();
      }else{
        if (Left == null)
            L = null;
        else
            Left.Split(x, out L, out newTree);
        R = new ImplicitTreap(y, Cost, newTree, Right);
        R.Recalc();
      }
    }
////////////////////////////////////////////////////////
  public:
    class TIterator{
      friend class TTreap;
      TTreap *Owner;
      size_t idx;
    };
};
/*
insert(idx,value) erase(idx) operator [](idx) push pop back
TCartesianTree - по неявному ключу
1. Параметризуется типом элементов.
2. Должен быть конструктор по-умолчанию, деструктор, конструктор копирования, оператор присваивания.
3. Методы: push_back(value), push_front(value), insert(idx, value), operator [] (idx).
4. Тесты: вставка в края, в середину, нагрузочное тестирование.
*/
int main()
{
    uint8 t=12;
    cout << t << endl;
    return 0;
}

