#include <iostream>
#include <cstdlib>
#include <ctime>
#include "vector"
#include <algorithm>
#include <limits.h>
using namespace std;
typedef vector< vector< int > > TIntMatrix;

void GenMatrix(TIntMatrix &A){
    srand(time(NULL));
    for (size_t i=0;i<10;i++){
      A.push_back(vector< int >());
      for (size_t j=0;j<10;j++){
        A[i].push_back(rand() %10 + 0);
        cout<<A[i][j]<<' ';
      }
      cout<<'\n';
    }
    cout<<'\n';
}

int FindMax(const vector<int> &a){
  int max=a[0];
  for (size_t i=1;i<a.size();i++) if (a[i]>max) max=a[i];
  return max;
}

bool CmpMax(const int &a){
    return a==1;
}

void write(const vector<vector<int> > &M){
for (size_t i=1;i<M.size();i++){
        for (size_t j=1;j<M[i].size();j++){
            cout<<M[i][j]<<' ';
        }
        cout<<'\n';
    }
}
/////////////////////////////////////////////////////////////////
struct Cmp{
  int Value; size_t Num,Found;

  Cmp(int v,size_t k):Value(v),Num(k),Found(0)
  {}

  bool operator()(int a){
    if (a==Value){
      if (++Found==Num) return true;
    }
  return false;
  }

};


struct TFind{
  int K;

  TFind(int k):K(k){}

  bool operator ()(const vector<int> &line)const{
    return find(line.begin(),line.end(),K)!=line.end();
  }

};

int main()
{
    TIntMatrix A; GenMatrix(A); TIntMatrix::const_iterator it1,it3;
    vector<int> B=A[0]; vector<int>::const_iterator it2;

    it1=find_if(A.begin(),A.end(),TFind(1));
    if (it1==A.end()){cout<<"Is out\n";}else{cout<<it1-A.begin()<<'\n';}

    it2=find_if(B.begin(),B.end(),Cmp(1,2)); //Выводит номер 2-ой единички в массиве
    if (it2==B.end()){cout<<"Is out\n";}else{cout<<it2-B.begin()<<'\n';}

    //sort(B.begin(),B.end(),[](const int *a, const int *b){a<b;}); //Лямбда-выражение
    for_each (B.begin(),B.end(),[](size_t i){cout<<i<<' ';});
    //for_each (myvector.begin(), myvector.end(), myobject);
    return 0;
}
