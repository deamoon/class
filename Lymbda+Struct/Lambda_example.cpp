#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>

const size_t N = 10;

typedef std::vector< std::vector<int> > TMatrix;

void Fill(TMatrix &A) {
    A.clear();
    for (size_t i = 0; i < N; ++i) {
        A.push_back(std::vector<int>());
        for (size_t j = 0; j < N; ++j)
            A[i].push_back(rand() % 100);
    }
}

void Print(const TMatrix &A) {
    std::for_each(A.begin(), A.end(), [] (const std::vector<int> &line) {
        const int *beg = &*line.begin();
        std::for_each(line.begin(), line.end(), [beg] (const int &v) {
            if (&v != beg) std::cout << ' ';
            std::cout << v;
        });
        std::cout << std::endl;
    });
}

int main() {
    srand(time(NULL));
    TMatrix A;
    Fill(A);
    Print(A);
    TMatrix::const_iterator it = std::find_if(A.begin(), A.end(), [] (const std::vector<int> &line) {
        return std::find(line.begin(), line.end(), 1) != line.end();
    });
    if (it != A.end()) {
        std::cout << "Found at index " << it - A.begin() << std::endl;
    } else {
        std::cout << "Not found" << std::endl;
    }
    return 0;
}

