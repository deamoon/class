#include <iostream>
#include <math.h>
#include <cstdlib>
#include <limits>
#include <ctime>
using namespace std;

template <typename TKey, typename TData>
class TAVL{
  private:
    struct TNode{
      TData Data;
      TNode *Parent;
      TNode *Left;
      TNode *Right;
      int Height;
      TKey Key;
      TNode(){}
      TNode(TKey k, TData d){ Key = k; Parent=Left=Right=NULL; Height=1; Data=d;}
    };
    TNode *Root;
  int height(TNode *p){
    if (!p) return 0; else return p->Height;
  }
  void fixheight(TNode *p){
    int l=height(p->Left);
    int r=height(p->Right);
    if (l>r) p->Height = l+1; else p->Height = r+1;
  }
  int bfactor(TNode *p){
    return height(p->Right)-height(p->Left);
  }
  TNode* rotateright(TNode *p){
    TNode *q = p->Left;

    p->Left = q->Right;
    if (q->Right){
      q->Right->Parent = p;
    }
    q->Right = p;

    q->Parent = p->Parent;
    p->Parent = q;

    fixheight(p);
    fixheight(q);
    return q;
  }
  TNode* rotateleft(TNode *q){
    TNode *p = q->Right;

    q->Right = p->Left;
    if (p->Left){
      p->Left->Parent = q;
    }
    p->Left = q;

    p->Parent = q->Parent;
    q->Parent = p;

    fixheight(q);
    fixheight(p);
    return p;
  }
  TNode* balance(TNode *p){
    fixheight(p);
    if (bfactor(p)==2){
      if (bfactor(p->Right)<0)
        p->Right = rotateright(p->Right);
      return rotateleft(p);
    }
    if (bfactor(p)==-2){
      if (bfactor(p->Left)>0)
        p->Left = rotateleft(p->Left);
      return rotateright(p);
    }
    return p;
  }
  TNode* add(TNode* p, TKey k, TData d){
    if (!p) return new TNode(k,d);
    if (k < p->Key) {
      TNode *t; t = add(p->Left,k,d);
      p->Left = t; t->Parent = p;
    }else{
      TNode *t; t = add(p->Right,k,d);
      p->Right = t; t->Parent = p;
    }
    return balance(p);
  }
  TNode* findMin(TNode *p){
    while (p->Left) p = p->Left;
    return p;
  }
  TNode* findMax(TNode *p){
    while (p->Right) p = p->Right;
    return p;
  }
  TNode* next(TNode *p){
   TKey k = p->Key;
    while (k >= p->Key){
      if (!p->Parent) return NULL;
      p = p->Parent;
    }
    return p;
  }
  TNode* in_order(TNode *p){
    if (!p) return NULL;
    if ((p->Right==0)&&(p->Left==0)){ // Лист
      if (p->Parent->Left==p) return p->Parent; else // Левый лист
        return next(p);
    }else{
      return findMin(p->Right);
    }
  }
  void del(TNode *p){
    TNode *t; t=p;
    if (!t) return;
    if ((t->Right==0)&&(t->Left==0)){
      delete[] t;
    }else{
      TNode *t1 = t->Left, *t2 = t->Right;
      del(t1); //delete t1;
      del(t2); //delete t2;
      delete t;
    }
  }
  public:
  void del_user(){
    TNode *t; t=Root;
    if (!t) return;
    if ((t->Right==0)&&(t->Left==0)){
      delete[] t;
    }else{
      TNode *t1 = t->Left, *t2 = t->Right;
      del(t1); //delete t1;
      del(t2); //delete t2;
      delete t;
    }
    Root=NULL;
  }
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////
  class TIterator{
    friend class TAVL;
    TNode *Ptr;

    TNode* findMin(TNode *p){
      while (p->Left) p = p->Left;
      return p;
    }
    TNode* next(TNode *p){
     TKey k = p->Key;
      while (k >= p->Key){
        if (!p->Parent) return NULL;
        p = p->Parent;
      }
      return p;
    }
    TNode* in_order(TNode *p){
      if (!p) return NULL;
      if ((p->Right==0)&&(p->Left==0)){ // Лист
        if (p->Parent->Left==p) return p->Parent; else // Левый лист
          return next(p);
      }else{
        if (p->Right) return findMin(p->Right); else return p;
      }
    }

    public:
    TData data(TIterator &a){
      return a.Ptr->Data;
    }
    TKey & operator * (){
      return Ptr->Key;
    }
    TIterator & operator ++ (){
      Ptr = in_order(Ptr);
      return *this;
    }
    bool operator == (const TIterator &a) const{
      return Ptr==a.Ptr;
    }
    TIterator (TNode *p)
      :Ptr(p){
    }
  };
  TIterator begin(){
    TIterator res(findMin(Root));
    return res;
  }
  TIterator end(){
    TNode *p = Root;
    while (p->Right) p = p->Right;
    TIterator res(p);
    return res;
  }
                              /////////////////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

  TData operator [] (const TKey k) const{
    TNode *p = Root;
    while (p){
      if (k<p->Key)  p = p->Left; else
        if (k>p->Key)  p = p->Right; else
          if (k==p->Key) return p->Data;
    }
    return -1;
  }
  TAVL(){
    Root = NULL;
  }
  TAVL (const TAVL& t) {
    Root = NULL; TNode *max = findMax(t.Root);
    TIterator it1(findMin(t.Root));
    TIterator it2(max);
    for(;!(it1==it2);++it1){
      insert(it1.Ptr->Key,it1.Ptr->Data);
    }
    insert(max->Key,max->Data);
  }
  ~TAVL(){
    del(Root);
  }
  void Swap(TAVL &a){
    swap(Root,a.Root);
  }
  TAVL &operator =(const TAVL &l){
      TAVL tmp(l);
      Swap(tmp);
      return *this;
  }
  void insert(TKey key, TData data){
    Root = add(Root,key,data); Root->Parent = NULL;
  }
  void write_root(){
    cout<<"Root->Height: "<<Root->Height<<"\n";
    cout<<"Root->Data: "<<Root->Data<<"\n";
    if (Root->Left) cout<<"Root->Left->Data: "<<Root->Left->Data<<"\n";
    if (Root->Right) cout<<"Root->Right->Data: "<<Root->Right->Data<<"\n";
  }
};

/*
TAVLTree
1. Должен параметризоваться типом ключей и типом элементов.
2. Должен быть конструктор по-умолчанию, деструктор, конструктор копирования, оператор присваивания.
3. Должны быть методы: insert(key, value), operator [] (key), итерирование (begin(), end()).
4. Тесты: вставка в края, в середину, нагрузочное тестирование.
*/

int main()
{
  TAVL<int,int> a,b;

  for (int i=1;i<=10;++i) a.insert(i,i);

  TAVL<int,int>::TIterator it=a.begin();
  for(int i=1;i<=10;++i) {cout<<*it<<' '; ++it;}

  b=a;
  for (int i=11;i<=25;++i) b.insert(i,i);

  cout<<'\n'; it=b.begin();
  for(int i=1;i<=25;++i) {cout<<*it<<' '; ++it;}

  cout<<'\n'; it=a.begin();
  for(int i=1;i<=10;++i) {cout<<*it<<' '; ++it;}

  cout<<'\n'; a.write_root();
  cout<<'\n'; b.write_root();

  a.insert(45,45);
  a.insert(245,245);
  a.insert(103,103);
  a.insert(156,156);

  cout<<'\n'; it=a.begin();
  for(int i=1;i<=14;++i) {cout<<*it<<' '; ++it;}

  cout<<'\n'<<a[245]<<' '<<a[45]<<' '<<a[57]<<'\n';

  srand(time(NULL)); int ran;
  for(size_t i=0;i<=1023;++i) {
    ran = rand() % 2000;
    a.insert(ran, ran);
  }

  cout<<'\n';
  a.write_root();
  cout<<'\n'; it=a.begin();
  for(int i=1;i<=20;++i) {cout<<*it<<' '; ++it;}

  cout<<"\n\n";
  a.del_user();
  for(size_t i=0;i<=1023;++i) {
    ran = rand() % 2000;
    a.insert(ran, ran);
  }
  a.write_root();
  cout<<'\n'; it=a.begin();
  for(int i=1;i<=20;++i) {cout<<*it<<' '; ++it;}

  return 0;
}
